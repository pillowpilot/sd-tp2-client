package py.una.pol;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;
import java.util.Scanner;

public class App
{
    final static String urlString = "http://localhost:8080/personas/rest/"; // Es necesario la barra ( / ) final

    public static void main(String[] args)
            throws JsonProcessingException
    {
        Scanner in = new Scanner(System.in);


        int option;
        do {
            printMenuTitle();
            option = in.nextInt();
            switch (option)
            {
                case 1: // createPerson
                    Person person = readPerson(in);
                    createPerson(person);
                    break;
                case 2: // createSubject
                    Subject subject = readSubject(in);
                    createSubject(subject);
                    break;
                case 3: // listPeople
                    listPeople();
                    break;
                case 4: // listPerson'sSubjects
                    Long personId = readPersonId(in);
                    listPersonSubjects(personId);
                    break;
                default:
                    System.out.println("Opcion no valida!");
                    break;
            }
        }while(option != 0);

    }

    private static void listPeople()
    {
        String path = "personas";
        sendRequest(path);
    }

    private static void listPersonSubjects(Long id)
    {
        String path = "personas/" + id + "/asignaturas";
        sendRequest(path);
    }

    private static void createPerson(Person person)
            throws JsonProcessingException
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        String jsonPerson = jsonMapper.writeValueAsString(person);
        sendRequest("personas/", "POST", Optional.of(jsonPerson));
    }

    private static void createSubject(Subject subject)
            throws JsonProcessingException
    {
        ObjectMapper jsonMapper = new ObjectMapper();
        String jsonSubject = jsonMapper.writeValueAsString(subject);
        sendRequest("asignaturas/", "POST", Optional.of(jsonSubject));
    }

    private static void sendRequest(String path, String method, Optional<String> jsonString)
    {
        try {
            URL url = new URL(urlString + path);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestMethod(method);

            if (jsonString.isPresent()) {
                conn.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(jsonString.get());
                wr.flush();
                wr.close();
            }

            if (conn.getResponseCode() < 200 || conn.getResponseCode() >= 300) {
                throw new RuntimeException("Error HTTP - código : " + conn.getResponseCode() + " : " + conn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Impresión del contenido de la respuesta: \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
            }

            conn.disconnect();
        } catch (IOException|RuntimeException e) {
            System.out.println(e.getMessage());
            System.out.println("Continuando...");
            System.out.println();
        }
    }

    private static void sendRequest(String path)
    {
        sendRequest(path, "GET", Optional.empty());
    }

    private static void printMenuTitle()
    {
        System.out.println("Cliente SD - TP2");
        System.out.println("Optiones:");
        System.out.println("1. Crear persona.");
        System.out.println("2. Crear asignatura.");
        System.out.println("3. Listar personas.");
        System.out.println("4. Listar asignaturas de persona.");


        System.out.print("Ingrese la opcion: ");
    }

    private static Person readPerson(Scanner in)
    {
        System.out.println("Ingrese la cedula de la persona: ");
        Long id = in.nextLong();

        System.out.println("Ingrese el nombre de la persona: ");
        in.next(); // Consume last EOL
        String name = in.nextLine();

        System.out.println("Ingrese el apellido de la persona: ");
        String lastname = in.nextLine();

        Person person = new Person(id, name, lastname);
        return person;
    }

    private static Subject readSubject(Scanner in)
    {
        System.out.println("Ingrese el id de la asignatura: ");
        Long id = in.nextLong();

        System.out.println("Ingrese el nombre de la asignatura: ");
        in.next(); // Consume last EOL
        String name = in.nextLine();

        Subject subject = new Subject(id, name);
        return subject;
    }

    private static Long readPersonId(Scanner in)
    {
        System.out.println("Ingrese la cedula de la persona: ");
        Long personId = in.nextLong();
        return personId;
    }
}