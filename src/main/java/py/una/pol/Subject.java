package py.una.pol;

public class Subject
{
    public Long id;
    public String nombre;

    public Subject(Long id, String nombre)
    {
        this.id = id;
        this.nombre = nombre;
    }
}
