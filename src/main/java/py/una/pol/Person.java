package py.una.pol;

public class Person
{
    public Long cedula;
    public String nombre;
    public String apellido;

    public Person(Long cedula, String nombre, String apellido)
    {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
    }
}
